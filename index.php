<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
  <div class="container">

    <div class="jumbotron">
        <h1 class="display-4">Hello, world!</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
    </div>

    <form action="index.php" method="post" class="mb-4">
        <div class="form-group">
            <label for="tache">tache</label>
            <input type="text" name="nouvelle_tache" class="form-control" id="tache">
            <small class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

    <?php 

    if ( isset($_POST['nouvelle_tache']) && !empty($_POST['nouvelle_tache']) ) {
        if ( !is_file('data/'.time().'.txt') ) {
            $fp = fopen('data/'.time().'.txt', 'w');
            fwrite($fp, $_POST['nouvelle_tache']);
            fclose($fp);
            echo '<div class="alert alert-success" role="alert">
                tache créée
            </div>';
        }
    }

    if ( isset($_GET['delete']) && is_file($_GET['delete']) ) {
        unlink($_GET['delete']);
        echo '<div class="alert alert-danger" role="alert">
            tache supprimée
        </div>';
    }

    ?>

    <h2>Liste de tâches (<?php echo count(glob("data/*.txt"));?>)</h2>

    <div class="row">
        <?php
        foreach (glob("data/*.txt") as $filename) {

            if ( filesize($filename) == 0 ) {
                $contents = "";
            } else {
                $handle = fopen($filename, "r");
                $contents = fread($handle, filesize($filename));
                fclose($handle);
            }            

            echo '
            <div class="col-lg-4 mb-4">
                <div class="card">
                    <div class="card-body">
                        <p class="card-text">'.$contents.'</p>
                        <a href="index.php?delete='.$filename.'" class="btn btn-warning btn-sm">supprimer la tache</a>
                    </div>
                </div>
            </div>';
        }
        ?>
    </div>

  </body>
</html>